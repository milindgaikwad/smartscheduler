package com.wondrx.smartscheduler

import com.wondrx.smartscheduler.entities.BreakTimeSlot
import com.wondrx.smartscheduler.entities.ConferenceRoom
import com.wondrx.smartscheduler.entities.ProductiveTimeSlot
import java.time.LocalTime

/**
 * A class to generate dummy ConferenceRoom data to test code
 *
 * @author Milind Gaikwad.
 */
class ConferenceDataGenerator {

    //To add static methods
    companion object {
        fun generateConferenceRoomData(): List<ConferenceRoom> {

            var productiveTimeSlot1 = ProductiveTimeSlot(
                LocalTime.of(9,0),
                LocalTime.of(12,0),"Talk")
            var breakTimeSlot = BreakTimeSlot(
                LocalTime.of(12,0),
                LocalTime.of(13,0),"Lunch")
            var productiveTimeSlot2 = ProductiveTimeSlot(
                LocalTime.of(13,0),
                LocalTime.of(17,0),"Talk")

            var timeSlots = listOf(productiveTimeSlot1,breakTimeSlot,productiveTimeSlot2)

            val conferenceRoom1 = ConferenceRoom(timeSlots, "Room 1")

            productiveTimeSlot1 = ProductiveTimeSlot(
                LocalTime.of(9,0),
                LocalTime.of(12,0),"Talk")
            breakTimeSlot = BreakTimeSlot(
                LocalTime.of(12,0),
                LocalTime.of(13,0),"Lunch")
            productiveTimeSlot2 = ProductiveTimeSlot(
                LocalTime.of(13,0),
                LocalTime.of(17,0),"Talk")

            timeSlots = listOf(productiveTimeSlot1,breakTimeSlot,productiveTimeSlot2)

            val conferenceRoom2 = ConferenceRoom(timeSlots, "Room 2")

            return listOf(conferenceRoom1,conferenceRoom2)
        }
    }
}