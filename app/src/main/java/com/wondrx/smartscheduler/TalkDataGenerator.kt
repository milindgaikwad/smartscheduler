package com.wondrx.smartscheduler

import com.wondrx.smartscheduler.entities.*

/**
 * A class to generate dummy Talks data to test code
 *
 * @author Milind Gaikwad
 */
class TalkDataGenerator {

    //To add static methods
    companion object {
        fun generateTalkData(): MutableList<Talk> {
            val talks: MutableList<Talk> = mutableListOf()
            talks.add(Talk("Writing Fast Tests Against Enterprise Rails",60))
            talks.add(Talk("Overdoing it in Python",45))
            talks.add(Talk("Lua for the Masses",30))
            talks.add(Talk("Ruby Errors from Mismatched Gem Versions",45))
            talks.add(Talk("Common Ruby Errors",45))
            talks.add(Talk("Rails for Python Developers",5))
            talks.add(Talk("Communicating Over Distance",60))
            talks.add(Talk("Accounting-Driven Development",45))
            talks.add(Talk("Woah",30))
            talks.add(Talk("Sit Down and Write",30))
            talks.add(Talk("Pair Programming vs Noise",45))
            talks.add(Talk("Rails Magic",60))
            talks.add(Talk("Ruby on Rails: Why We Should Move On",60))
            talks.add(Talk("Clojure Ate Scala (on my project)",45))
            talks.add(Talk("Programming in the Boondocks of Seattle",30))
            talks.add(Talk("Ruby vs. Clojure for Back-End Development",30))
            talks.add(Talk("Ruby on Rails Legacy App Maintenance",60))
            talks.add(Talk("A World Without HackerNews",30))
            talks.add(Talk("User Interface CSS in Rails Apps",30))
            return talks
        }
    }
}