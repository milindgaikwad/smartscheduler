package com.wondrx.smartscheduler.entities

import com.wondrx.smartscheduler.util.TimeUtil
import java.time.Duration
import java.time.LocalTime

/**
 * Abstract class for representing a real world time interval
 * @param from : Start time of time interval
 * @param to : End time of time interval
 * @author Milind Gaikwad.
 */
abstract class TimeSlot(val from: LocalTime, val to: LocalTime) {

    /**
     * Time available for productive activities at any given time
     */
    private var availableTime: Long = Duration.between(from, to).toMinutes()

    /**
     * Tasks assigned to this time slot
     */
    private var assignedTalks: MutableList<Talk> = mutableListOf()

    fun isTimeAvailable(): Boolean {
        return availableTime > 0
    }

    fun howMuchTimeLeft(): Long {
        return availableTime
    }

    /**
     * Assign a task to the time slot
     */
    fun assignTalk(talk: Talk): Boolean {
        return if (availableTime >= talk.talkDurationInMin) {
            availableTime -= talk.talkDurationInMin
            assignedTalks.add(talk)
            true
        } else {
            false
        }
    }

    /**
     * Create a string representation of the schedule of this time slot
     */
    open fun printScheduleForTimeSlot(): String {
        return if (assignedTalks.isEmpty()) {
            ""
        } else {
            val scheduleBuilder = StringBuilder()
            var currentTime = LocalTime.of(from.hour, from.minute)
            assignedTalks.forEach { talk ->
                scheduleBuilder.append(currentTime.format(TimeUtil.dtf)).append(" ")
                    .append(talk.toString()).append(" \n")
                currentTime = currentTime.plusMinutes(talk.talkDurationInMin)
            }
            scheduleBuilder.toString()
        }
    }

}