package com.wondrx.smartscheduler.entities

/**
 * Data class representing a real world Talk.
 * @param title : Talk title
 * @param talkDurationInMin : Duration of the talk in Minutes
 *
 * @author Milind Gaikwad.
 */
data class Talk(val title:String, val talkDurationInMin:Long) {
    override fun toString(): String {
        return title + " " + talkDurationInMin + "min"
    }
}

//Add priority when needed
