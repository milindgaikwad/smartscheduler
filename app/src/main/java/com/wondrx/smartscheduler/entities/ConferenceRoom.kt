package com.wondrx.smartscheduler.entities

/**
 * Represents a real world conference room.
 *
 * @param timeSlots: All available time slots.
 * @param name : Name of the room.
 *
 * @author Milind Gaikwad.
 */
data class ConferenceRoom(var timeSlots: List<TimeSlot>, val name: String) {

    fun isProductiveTimeAvailable(): Boolean {
        timeSlots.forEach {
            if (it.isTimeAvailable()) {
                return true
            }
        }
        return false
    }

    fun howMuchTimeIsAvailable(): Long {
        var availableTimeInRoom: Long = 0
        timeSlots.forEach {
            availableTimeInRoom += (it.howMuchTimeLeft())
        }
        return availableTimeInRoom
    }

    /**
     * Create a string representation of the schedule of this conference room
     */
    fun printRoomSchedule(): String {
        val scheduleBuilder = StringBuilder()
        scheduleBuilder.append("$name: \n")
        timeSlots.forEach {
            scheduleBuilder.append(it.printScheduleForTimeSlot())
        }
        return scheduleBuilder.toString()
    }

    /**
     * Add talks to the schedule
     */
    fun scheduleTalks(talks: MutableList<Talk>): MutableList<Talk> {

        var assignedTalks: MutableList<Talk> = mutableListOf()

        timeSlots.forEach { timeSlot ->
            if (timeSlot is ProductiveTimeSlot) {
                talks.forEach { talk ->
                    if (timeSlot.assignTalk(talk)) {
                        assignedTalks.add(talk)
                    }
                }
                removeAssignedTasksFromTaskList(assignedTalks, talks)
                assignedTalks = mutableListOf()
            }
        }

        return talks
    }

    /**
     * Remove assigned talks for talks master list
     */
    private fun removeAssignedTasksFromTaskList(assignedTalks: MutableList<Talk>,
                                                talks: MutableList<Talk>) {
        assignedTalks.forEach {
            talks.remove(it)
        }
    }

}