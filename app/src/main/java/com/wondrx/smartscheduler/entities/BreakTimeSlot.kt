package com.wondrx.smartscheduler.entities

import com.wondrx.smartscheduler.Scheduler
import com.wondrx.smartscheduler.util.TimeUtil
import java.time.LocalTime

/**
 * Sub class of
 * @see com.wondrx.smartscheduler.entities.TimeSlot
 *
 * Represents a break form productive activities.
 *
 * @param description : Description of the break.
 *
 * @author Milind Gaikwad
 */
class BreakTimeSlot(from: LocalTime, to: LocalTime, private val description: String) : TimeSlot(from, to) {
    override fun printScheduleForTimeSlot(): String {
        return StringBuilder().append(from.format(TimeUtil.dtf)).append(" ").append(description)
            .append(" \n").toString()
    }
}