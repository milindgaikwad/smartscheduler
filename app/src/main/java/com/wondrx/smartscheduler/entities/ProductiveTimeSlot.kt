package com.wondrx.smartscheduler.entities

import java.time.LocalTime

/**
 * Sub class of
 * @see com.wondrx.smartscheduler.entities.TimeSlot
 *
 * Represents a time duration within which productive activities can happen.
 *
 * @param description : Description of productive activity.
 *
 * @author Milind Gaikwad.
 */
class ProductiveTimeSlot(from: LocalTime, to: LocalTime, private val description:String) : TimeSlot(from, to) {

}