package com.wondrx.smartscheduler

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.TextView

/**
 * Primary activity in the app.
 *
 */
class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val txtInput: TextView = findViewById(R.id.txt_input)
        val btnGenerateSchedule : Button = findViewById(R.id.btn_schedule)

        btnGenerateSchedule.setOnClickListener {
            //Create schedule
            val schedule = Scheduler.scheduleTalks(TalkDataGenerator.generateTalkData(),ConferenceDataGenerator.generateConferenceRoomData())
            txtInput.text = schedule
            //Hide button as it has served it's purpose
            btnGenerateSchedule.visibility = View.INVISIBLE
        }

    }
}