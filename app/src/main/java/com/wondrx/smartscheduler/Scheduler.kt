package com.wondrx.smartscheduler

import com.wondrx.smartscheduler.entities.ConferenceRoom
import com.wondrx.smartscheduler.entities.Talk

/**
 * A class that holds logic to create a schedule according to given data.
 *
 * @author Milind Gaikwad.
 */
class Scheduler {

    //to add static methods
    companion object {

        fun scheduleTalks(talks: MutableList<Talk>, conferenceRooms: List<ConferenceRoom>): String {
            var localTalksList: MutableList<Talk> = mutableListOf()
            localTalksList.addAll(talks)
            localTalksList.sortByDescending { it.talkDurationInMin }
            val scheduleBuilder = StringBuilder()
            conferenceRooms.forEach { conferenceRoom ->
                localTalksList = conferenceRoom.scheduleTalks(localTalksList)
                scheduleBuilder.append(conferenceRoom.printRoomSchedule())
            }
            return scheduleBuilder.toString()
        }
    }
}