package com.wondrx.smartscheduler.util

import java.time.format.DateTimeFormatter
import java.util.*

/**
 * A util class for time related operations
 *
 * @author Milind Gaikwad.
 */
class TimeUtil {
    companion object {
        var dtf: DateTimeFormatter = DateTimeFormatter.ofPattern("hh:mm a").withLocale(Locale.US)
    }
}